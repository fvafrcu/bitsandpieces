#!/bin/dash
# based on https://cran.r-project.org/bin/linux/debian/#installing-r-devel-or-a-release-branch-from-svn
export RTOP=~/svn/R
mkdir -p $RTOP
export REPOS=https://svn.r-project.org/R
cd $RTOP

# get/update source
if [ -d r-devel ]; then 
    cd $RTOP/r-devel/source
    svn up
else 
    svn co $REPOS/trunk r-devel/source
    mkdir $RTOP/r-devel/build
fi

# update recommended
if [ $(hostname) == "fvafrdebianCU" ]; then
    cd $RTOP/r-devel/source/src/library/Recommended
    version=$(cut -f1 -d' ' ../../../VERSION)
    rm *tgz
    rm *tar.gz
    rm index.html
    wget https://cran.r-project.org/src/contrib/${version}/Recommended/
    files=$(grep tar.gz index.html | cut -d'>' -f7 | cut -d'<' -f1)
    for file in $files
    do
      wget https://cran.r-project.org/src/contrib/${version}/Recommended/$file
    done
    echo "Creating links"
    rm -f *.tgz
    PKGS=$(ls  *tar.gz | cut -f1 -d"_")
    for i in ${PKGS} ; do
      ln -s $i*.tar.gz ${i}.tgz
    done
else
    cd $RTOP/r-devel/source
    ./tools/rsync-recommended
fi

cd $RTOP/r-devel/build 
../source/configure
make

if true; then
    R-devel -e 'update.packages(ask = FALSE)'
else 
    ln -s $RTOP/r-devel/build/bin/R ~/bin/R-devel
    ln -s $RTOP/r-devel/build/bin/Rscript ~/bin/Rscript-devel

    R-devel -e ' deps <- c("spelling", "remotes", 
    "cleanr", "document", "fakemake",
    "maSAE", "HandTill2001", "excerptr", 
    "rasciidoc", "packager",
    "microbenchmark", 
    "lintr", "methods", "utils", "crayon", "git2r", "tools", "whisker", "whoami",
    "cyclocomp", "httr", "digest", "hunspell", "MakefileR", "igraph", "graphics",
    "roxygen2", "callr", "rcmdcheck", "checkmate", "rstudioapi", "desc", "withr",
    "knitr", "rmarkdown", "devtools", "Rcpp", "igraph", "openssl", "rlang", "RUnit",
    "testthat", "datasets", "rprojroot", "covr", "rhub", "stringi", "devtools",
    "forestinventory"); for (d in deps) {if (! require(d, character.only = TRUE)) install.packages(d, character.only = TRUE)}' 
    R-devel -e 'remotes::install_gitlab("fvafrcu/cuutils")'
fi
